package com.fangbin.service.impl;

import com.fangbin.domain.User;
import com.fangbin.mapper.UserMapper;
import com.fangbin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: FangBin
 * @Description:
 * @Date: Created in 16:30 2021/4/12
 * @ModifiedBy:
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> listUser() {
        return userMapper.listUser();
    }

    @Override
    public User findByIdUser(Integer id) {
        return userMapper.findByIdUser(id);
    }

    @Override
    public Boolean removeUser(Integer id) {
        return userMapper.removeUser(id);
    }

    @Override
    public Boolean insertUser(User user) {
        return userMapper.insertUser(user);
    }

    @Override
    public Boolean updateUser(User user) {
        return userMapper.updateUser(user);
    }
}
