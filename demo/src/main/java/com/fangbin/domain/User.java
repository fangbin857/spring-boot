package com.fangbin.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: FangBin
 * @Description:
 * @Date: Created in 16:28 2021/4/12
 * @ModifiedBy:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id;
    private String username;
    private Integer age;
}
