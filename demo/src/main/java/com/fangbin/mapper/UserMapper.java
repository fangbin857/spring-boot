package com.fangbin.mapper;

import com.fangbin.domain.User;

import java.util.List;

/**
 * @Author: FangBin
 * @Description:
 * @Date: Created in 16:31 2021/4/12
 * @ModifiedBy:
 */
public interface UserMapper {

    List<User> listUser();

    User findByIdUser(Integer id);

    Boolean removeUser(Integer id);

    Boolean insertUser(User user);

    Boolean updateUser(User user);
}
