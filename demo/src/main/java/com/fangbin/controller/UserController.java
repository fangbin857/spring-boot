package com.fangbin.controller;

import com.alibaba.fastjson.JSON;
import com.fangbin.domain.User;
import com.fangbin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: FangBin
 * @Description:
 * @Date: Created in 16:29 2021/4/12
 * @ModifiedBy:
 */
@RestController
@RequestMapping("/testBoot")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * @Description: 查询所有
     * @param: []
     * @return: java.lang.String
     * @Author: FangBin
     * @Date: 2021/4/12
     */
    @RequestMapping("/list")
    public String listUser() {
        List<User> users = userService.listUser();
        return JSON.toJSONString(users);
    }

    /**
     * @Description: 根据id查询
     * @param: [id]
     * @return: java.lang.String
     * @Author: FangBin
     * @Date: 2021/4/12
     */
    @RequestMapping("/find/{id}")
    public String findByIdUser(@PathVariable Integer id) {
        User user = userService.findByIdUser(id);
        return JSON.toJSONString(user);
    }

    /**
     * @Description: 根据id删除
     * @param: [id]
     * @return: java.lang.String
     * @Author: FangBin
     * @Date: 2021/4/12
     */
    @RequestMapping("/remove/{id}")
    public String removeUser(@PathVariable Integer id) {
        if (!userService.removeUser(id)) {
            return "删除失败";
        }
        return "删除成功";
    }

    /**
     * @Description: 添加用户
     * @param: [username, age]
     * @return: java.lang.Boolean
     * @Author: FangBin
     * @Date: 2021/4/12
     */
    @RequestMapping("/insert/{username}/{age}")
    public String insertUser(@PathVariable String username,
                             @PathVariable Integer age) {
        User user = new User(null, username, age);
        if (!userService.insertUser(user)) {
            return "添加失败";
        }
        return "添加成功";
    }

    /**
     * @Description: 修改用户
     * @param: [username, age, id]
     * @return: java.lang.Boolean
     * @Author: FangBin
     * @Date: 2021/4/12
     */
    @RequestMapping("/update/{id}/{username}/{age}")
    public String updateUser(@PathVariable Integer id,
                             @PathVariable String username,
                             @PathVariable Integer age) {
        User user = new User(id, username, age);
        if (!userService.updateUser(user)) {
            return "修改失败";
        }
        return "修改成功";
    }
}
